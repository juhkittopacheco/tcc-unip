import { combineReducers } from "redux";
import infoUser from './infoUser';

export default combineReducers({
    infoUser
})