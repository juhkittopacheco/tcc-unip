const INITIAL_STATE = {
    user: "",
    password: "",
}

const infoUser = (state = INITIAL_STATE, action) => {

    switch (action.type) {
        case  'USER_SIGN_IN':

            return {...state, user: action.item.user, password: action.item.password};

        default:
            return state;
    }

}

export default infoUser;