import React, { useState } from 'react';
import { useHistory } from "react-router-dom";
import Swal from 'sweetalert2';
import { actions } from '../../store/actions/infoUser';
import { useDispatch } from 'react-redux';


const LoginFields = () => {
    const [user, setUser] = useState("");
    const [password, setPassword] = useState("");
    const history = useHistory();
    // const infoUser = useSelector(state => state.infoUser);
    const dispatch = useDispatch();
    

    const verifyLoginFields = (user, password) => {
        if (user === "") return false;
        if (password === "") return false;

        return true;
    }

    const logIn = async (e, user) => {
        e.preventDefault();

        if (!verifyLoginFields(user.user, user.password)) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Usuário ou senha inválido!',
            })
            return false
        };

        dispatch(actions.userSignIn({
            user:user.user, 
            password:user.password
        }));

        // #TODO Chamar a API de Login aqui
        return history.push("/home-user");
    }

    return (
        <div className="card">
            <div className="card-body login-card-body">
                <p className="login-box-msg">Clique em Entrar para iniciar sua sessão</p>
                <form action="" method="post" onSubmit={(e) => logIn(e, { user, password })}>
                    <div className="input-group mb-3">
                        <input data-testid="user" value={user} onChange={(e) => setUser(e.target.value)} type="text" className="form-control" placeholder="Usuário" />
                        <div className="input-group-append">
                            <div className="input-group-text">
                                <span className="fas fa-envelope" />
                            </div>
                        </div>
                    </div>
                    <div className="input-group mb-3">
                        <input data-testid="password" value={password} onChange={(e) => setPassword(e.target.value)} type="password" className="form-control" placeholder="Password" />
                        <div className="input-group-append">
                            <div className="input-group-text">
                                <span className="fas fa-lock" />
                            </div>
                        </div>
                    </div>
                    <div className="row mb-4">
                        <div className="col-8 mt-2">
                            <div className="custom-control custom-switch">
                                <input type="checkbox" className="custom-control-input" id="customSwitch1" />
                                <label className="custom-control-label" htmlFor="customSwitch1">Entrar como hospital</label>
                            </div>
                        </div>
                        {/* /.col */}
                        <div className="col-4">
                            <button data-testid="btn" type="submit" className="btn btn-primary btn-block">Entrar</button>
                        </div>
                        {/* /.col */}
                    </div>
                </form>
                <p className="mb-0">
                    <a href="register.html" className="text-center">Ainda não possuo cadastro</a>
                </p>
            </div>
        </div>

    );
}




export default LoginFields;
