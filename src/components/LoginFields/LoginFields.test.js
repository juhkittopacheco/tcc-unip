import React from 'react';
import ReactDOM from 'react-dom';
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import LoginFields from './index';
import { store } from '../../store/';
import { Provider } from 'react-redux';


describe('Testa o component LoginFields', () => {
  it('deve renderizar o componente sem erros', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Provider store={store}><LoginFields /></Provider>, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('testa se existe validação no campo usuário', () => {
    const { getByTestId, getByText } = render(<Provider store={store}><LoginFields /></Provider>);
    const inputPassword = getByTestId('password');
    fireEvent.change(inputPassword, { target: { value: 'currentPassword' } });
    fireEvent.click(getByTestId('btn'));
    expect(getByText('Oops...')).toBeInTheDocument();
  });

  it('testa se existe validação no campo senha', () => {
    const { getByTestId, getByText } = render(<Provider store={store}><LoginFields /></Provider>);
    const inputUser = getByTestId('user');
    fireEvent.change(inputUser, { target: { value: 'currentUser' } });
    fireEvent.click(getByTestId('btn'));
    expect(getByText('Oops...')).toBeInTheDocument();
  });

  it('testa se esta logando', () => {
    const { getByTestId } = render(<Provider store={store}><LoginFields /></Provider>);
    const inputUser = getByTestId('user');
    const inputPassword = getByTestId('password');
    global.window = { location: { pathname: null } };

    fireEvent.change(inputUser, { target: { value: 'currentUser' } });
    fireEvent.change(inputPassword, { target: { value: 'currentPassword' } });
    fireEvent.click(getByTestId('btn'));
    expect(global.window.location.pathname).toEqual('/');
  });

});