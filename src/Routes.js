import React from "react";
import { BrowserRouter, Route, Switch} from "react-router-dom";

// import { isAuthenticated } from "./services/Auth";

import "./services/Links";
// import "./services/Scripts";

import Login from './pages/authentication/Login';
import HomeUser from './pages/user/HomeUser';
import Profile from './pages/user/Profile';
import VaccineUser from './pages/user/VaccineUser';
import VaccineCreateUser from './pages/user/VaccineCreateUser';
import CalendarComponent from './pages/user/CalendarComponent';
import Campaign from './pages/hospital/Campaign';
import Campaigns from './pages/hospital/Campaigns';
import CreateCampaigns from './pages/hospital/CreateCampaigns';
import Users from './pages/hospital/Users';
import RegisterUser from './pages/register/RegisterUser';
import RegisterHospital from './pages/register/RegisterHospital';



// const PrivateRoute = ({ component: Component, ...rest }) => (
    
//   <Route {...rest} render={props =>
//       isAuthenticated() ? (

//         <Component {...props}  />

//       ) : (
//         <Redirect to={{ pathname: "/login", state: { from: props.location } }} />
//       )
//     }
//   />
// );

const Routes = () => 

   (
    <BrowserRouter basename="/">
      
      {


        <Switch>
          <Route exact={true} path="/" component={Login} />
          <Route exact={true} path="/login" component={Login} />
          <Route exact={true} path="/caledndar" component={CalendarComponent} />
          <Route exact={true} path="/campaigns" component={Campaigns} />
          <Route exact={true} path="/campaign" component={Campaign} />
          <Route exact={true} path="/users" component={Users} />
          <Route exact={true} path="/create-campaigns" component={CreateCampaigns} />
          <Route exact={true} path="/vaccine-create-user" component={VaccineCreateUser} />
          <Route exact={true} path="/vaccine-user" component={VaccineUser} />
          <Route exact={true} path="/profile" component={Profile} />
          <Route exact={true} path="/home-user" component={HomeUser} />
          <Route exact={true} path="/registerUser" component={RegisterUser} />
          <Route exact={true} path="/registerHospital" component={RegisterHospital} />
          <Route path="*" component={() => <h1>Página não encontrada!</h1>} />
        </Switch>
      }
      
      
    </BrowserRouter>
  );


      

export default Routes;