import React from 'react';

function RegisterHospital() {
    return (
        <div className="hold-transition login-page">
            <div className="login-box">
            <div className="login-logo">
                <a href="./index2.html"><b>Minhas</b>Vacinas</a>
            </div>
  {/* /.login-logo */}
  <div className="card">
    <div className="card-body login-card-body">
      <div className="text-center">
        <div className="form-group">
          <img 
          className="profile-user-img img-fluid img-circle" 
          src={require("../../../dist/img/hospital.png").default}
          alt="User profile picture" />
          <div className="input-group">
            <div><input type="file" accept="image/x-png,image/gif,image/jpeg" encType="multipart/form-data" className="custom-file-input" id="exampleInputFile" /><label className=" w-100 btn btn-primary ml-auto mr-auto" htmlFor="exampleInputFile">Selecionar uma imagem</label></div>
          </div>
        </div>
      </div>
      <form action="./index3.html" method="post">
        <div className="form-group">
          <div className="mb-3">
            <label>Usuario:</label>
            <input type="text" className="form-control" placeholder />
          </div>
          <div className="mb-3">
            <label>Senha:</label>
            <input type="password" className="form-control" placeholder />
          </div>
          <div className="mb-3">
            <label>Código Verificador:</label>
            <input type="text" className="form-control" placeholder />
          </div>
          <div className="mb-3">
            <label>Nome do hospital:</label>
            <input type="text" className="form-control" placeholder />
          </div>
          <div className="mb-3">
            <label>Endereço:</label>
            <input type="text" className="form-control" placeholder />
          </div>
          <div className="row row d-flex flex-row-reverse">
            {/* /.col */}
            <div className="col-4">
              <button type="submit" className="btn btn-primary btn-block">Entrar</button>
            </div>
            {/* /.col */}
          </div>
          <p className="mb-0">
            <a href="register.html" className="text-center">Voltar</a>
          </p>
        </div></form>
      {/* /.login-card-body */}
    </div>
  </div>
</div>

        </div>

    );
}

export default RegisterHospital;
