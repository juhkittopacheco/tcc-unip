import React from 'react';
import Menu from '../../../components/Menu'
import Select from 'react-select'


const options = [
    { value: '0', label: 'Ovo' },
    { value: '9', label: 'Gelatina' }
]

function Profile() {
    return (
        <div className="wrapper">
            <nav className="main-header navbar navbar-expand navbar-white navbar-light">
            {/* Left navbar links */}
            <ul className="navbar-nav">
                <li className="nav-item">
                <a className="nav-link" data-widget="pushmenu" href="#" role="button"><i className="fas fa-bars" /></a>
                </li>
            </ul>
            </nav>
            {/* Main Sidebar Container */}
            <Menu/>
            <div className="content-wrapper">
            {/* Content Header (Page header) */}
            <section className="content-header">
                <div className="container-fluid">
                <div className="row mb-2">
                    <div className="col-sm-6">
                    <h1>Perfil</h1>
                    </div>
                    <div className="col-sm-6">
                    <ol className="breadcrumb float-sm-right">
                        <li className="breadcrumb-item"><a href="#">Home</a></li>
                        <li className="breadcrumb-item active">Perfil</li>
                    </ol>
                    </div>
                </div>
                </div>
                {/* /.container-fluid */}
            </section>
            {/* Main content */}
            <section className="content">
                <div className="container-fluid">
                <div className="row">
                    <div className="col-md-3">
                    {/* Profile Image */}
                    <div className="card card-primary card-outline">
                        <div className="card-body box-profile">
                        <div className="text-center">
                            <img 
                                className="profile-user-img img-fluid img-circle"
                                src={require("../../../dist/img/man.png").default} 
                                alt="User profile picture" />
                        </div>
                        <h3 className="profile-username text-center">Alexander Pierce</h3>
                        <p className="text-muted text-center">23 anos</p>
                        <a href="#" className="btn btn-primary btn-block"><b>Trocar foto</b></a>
                        </div>
                        {/* /.card-body */}
                    </div>
                    {/* /.card */}
                    </div>
                    {/* /.col */}
                    <div className="col-md-9">
                    <div className="card">
                        <div className="card-header p-2">
                        <ul className="nav nav-pills">
                            <li className="nav-item"><a className="nav-link active" href="#settings" data-toggle="tab">Seus dados</a></li>
                        </ul>
                        </div>
                        {/* /.card-header */}
                        <div className="card-body">
                        <div className="tab-content">
                            <div className="tab-pane active" id="settings">
                            <form className="form-horizontal">
                                <div className="form-group row">
                                <label className="col-sm-2 col-form-label">Usuario:</label>
                                <div className="col-sm-10">
                                    <input defaultValue="PFrancisca" type="text" className="form-control" placeholder />
                                </div>
                                </div>
                                <div className="form-group row">
                                <label className="col-sm-2 col-form-label">Senha:</label>
                                <div className="col-sm-10">
                                    <input defaultValue="Alexander Pierce" type="password" className="form-control" placeholder />
                                </div>
                                </div>
                                <div className="form-group row">
                                <label className="col-sm-2 col-form-label">Nome completo:</label>
                                <div className="col-sm-10">
                                    <input defaultValue="Alexander Pierce" type="text" className="form-control" placeholder />
                                </div>
                                </div>
                                <div className="form-group row">
                                <label className="col-sm-2 col-form-label">CPF:</label>
                                <div className="col-sm-10">
                                    <input defaultValue="123.456.789-09" type="text" className="form-control" placeholder />
                                </div>
                                </div>
                                <div className="form-group row">
                                <label className="col-sm-2 col-form-label">Endereço:</label>
                                <div className="col-sm-10">
                                    <input defaultValue="Sp, Barueri - Av. Ameida Faria, 769" type="text" className="form-control" placeholder />
                                </div>
                                </div>
                                <div className="form-group row">
                                <label className="col-sm-2 col-form-label">Data de Nascimento:</label>
                                <div className="col-sm-10">
                                    <input defaultValue="1997-04-29" type="date" className="form-control" placeholder />
                                </div>
                                </div>
                                <div className="form-group row">
                                <label className="col-sm-2 col-form-label">Selecione suas alergias:</label>
                                <div className="col-sm-10">
                                <div className="select2-purple">
                                    <Select label="Selecione" title="Selecione" className="select2" options={options} style={{width: '100%'}} isMulti={true} />
                                </div>
                                </div>
                                </div>
                                <div className="row d-flex flex-row-reverse">
                                <button type="submit" className="btn btn-danger">Salvar</button>
                                </div>
                            </form>
                            </div>
                            {/* /.tab-pane */}
                        </div>
                        {/* /.tab-content */}
                        </div>
                        {/* /.card-body */}
                    </div>
                    {/* /.card */}
                    </div>
                    {/* /.col */}
                </div>
                {/* /.row */}
                </div>
                {/* /.container-fluid */}
            </section>
            {/* /.content */}
            </div>
            {/* Control Sidebar */}
            <aside className="control-sidebar control-sidebar-dark">
            {/* Control sidebar content goes here */}
            </aside>
            {/* /.control-sidebar */}
        </div>
      
    );
}

export default Profile;
