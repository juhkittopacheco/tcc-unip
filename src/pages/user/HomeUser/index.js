import React from 'react';
import Menu from '../../../components/Menu'

function HomeUser() {
    return (
            <div className="wrapper">
            {/* Navbar */}
            <nav className="main-header navbar navbar-expand navbar-white navbar-light">
                {/* Left navbar links */}
                <ul className="navbar-nav">
                <li className="nav-item">
                    <a className="nav-link" data-widget="pushmenu" href="#" role="button"><i className="fas fa-bars" /></a>
                </li>
                </ul>
            </nav>
            {/* /.navbar */}
            {/* Main Sidebar Container */}
            <Menu/>
            <div className="content-wrapper">
                {/* Content Header (Page header) */}
                {/* Main content */}
                <section className="content">
                {/* Default box */}
                <div className="row d-flex justify-content-center">
                    <div className="card col-9 mt-1 p-2">
                    <img style={{width: '100%', height: 'auto'}} src={require("../../../dist/img/unnamed.png").default} />
                    {/* /.card-footer*/}
                    </div>
                    {/* /.card */}
                </div>
                <div className="row d-flex justify-content-center">
                    <div className="col-md-9">
                    {/* Box Comment */}
                    <div className="card card-widget">
                        <div className="card-header">
                        <div className="user-block">
                            <img className="img-circle" src={require("../../../dist/img/hospital.png").default} alt="User Image" />
                            <span className="username"><a href="#">Hospital josé</a></span>
                            <span className="description">Shared publicly - 7:30 PM Today</span>
                        </div>
                        {/* /.user-block */}
                        <div className="card-tools">
                            <button type="button" className="btn btn-tool" data-toggle="tooltip" title="Mark as read">
                            <i className="far fa-circle" /></button>
                            <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus" />
                            </button>
                            <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times" />
                            </button>
                        </div>
                        {/* /.card-tools */}
                        </div>
                        {/* /.card-header */}
                        <div className="card-body" style={{display: 'block'}}>
                        <img className="img-fluid pad" src={require("../../../dist/img/banner_vacinacao_sarampo_fev2020_site.png").default} alt="Photo" />
                        <p>I took this photo this morning. What do you guys think?</p>
                        </div>
                    </div>
                    {/* /.card */}
                    </div>
                    <div className="col-md-9">
                    {/* Box Comment */}
                    <div className="card card-widget">
                        <div className="card-header">
                        <div className="user-block">
                            <img className="img-circle" src={require("../../../dist/img/man.png").default} alt="User Image" />
                            <span className="username"><a href="#">Prefeitura</a></span>
                            <span className="description">Shared publicly - 7:30 PM Today</span>
                        </div>
                        {/* /.user-block */}
                        <div className="card-tools">
                            <button type="button" className="btn btn-tool" data-toggle="tooltip" title="Mark as read">
                            <i className="far fa-circle" /></button>
                            <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus" />
                            </button>
                            <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times" />
                            </button>
                        </div>
                        {/* /.card-tools */}
                        </div>
                        {/* /.card-header */}
                        <div className="card-body">
                        {/* post text */}
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at</p>
                        <p>the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into
                            your mouth.</p>
                        {/* Attachment */}
                        <div className="attachment-block clearfix">
                            <img className="attachment-img" src={require("../../../dist/img/banner-sarampo_1570370050.jpg").default} alt="Attachment Image" />
                            <div className="attachment-pushed">
                            <h4 className="attachment-heading"><a href="http://www.lipsum.com/">Lorem ipsum text generator</a></h4>
                            <div className="attachment-text">
                                Description about the attachment can be placed here. Lorem Ipsum is simply dummy text of the printing and typesetting industry... <a href="#">more</a>
                            </div>
                            {/* /.attachment-text */}
                            </div>
                            {/* /.attachment-pushed */}
                        </div>
                        {/* /.attachment-block */}
                        </div>
                    </div>
                    {/* /.card */}
                    </div>
                    <div className="col-md-9">
                    {/* Box Comment */}
                    <div className="card card-widget">
                        <div className="card-header">
                        <div className="user-block">
                            <img className="img-circle" src={require("../../../dist/img/man.png").default} alt="User Image" />
                            <span className="username"><a href="#">Prefeitura</a></span>
                            <span className="description">Shared publicly - 7:30 PM Today</span>
                        </div>
                        {/* /.user-block */}
                        <div className="card-tools">
                            <button type="button" className="btn btn-tool" data-toggle="tooltip" title="Mark as read">
                            <i className="far fa-circle" /></button>
                            <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus" />
                            </button>
                            <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times" />
                            </button>
                        </div>
                        {/* /.card-tools */}
                        </div>
                        {/* /.card-header */}
                        <div className="card-body">
                        {/* post text */}
                        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at</p>
                        <p>the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into
                            your mouth.</p>
                        {/* Attachment */}
                        <div className="attachment-block clearfix">
                            <img className="attachment-img" src={require("../../../dist/img/sarampo-18268083.jpg").default} alt="Attachment Image" />
                            <div className="attachment-pushed">
                            <h4 className="attachment-heading"><a href="http://www.lipsum.com/">Lorem ipsum text generator</a></h4>
                            <div className="attachment-text">
                                Description about the attachment can be placed here. Lorem Ipsum is simply dummy text of the printing and typesetting industry... <a href="#">more</a>
                            </div>
                            {/* /.attachment-text */}
                            </div>
                            {/* /.attachment-pushed */}
                        </div>
                        {/* /.attachment-block */}
                        </div>
                    </div>
                    {/* /.card */}
                    </div>
                </div>
                </section>
                {/* /.content */}
            </div>
            {/* /.content-wrapper */}
            {/* Control Sidebar */}
            <aside className="control-sidebar control-sidebar-dark">
                {/* Control sidebar content goes here */}
            </aside>
            {/* /.control-sidebar */}
            </div>


    );
}

export default HomeUser;
