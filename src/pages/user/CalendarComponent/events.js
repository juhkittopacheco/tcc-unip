const events = [
    {
      'title': 'Tuberculose (BCG)',
      'allDay': true,
      'start': new Date(2021, 1, 26),
      'end': new Date(2021, 1, 27)
    },
    {
        'title': 'Poliomielite ou Paralisia Infantil (VOP)',
        'start': new Date(2021, 1, 2),
        'end': new Date(2021, 1, 5)
    },
    {
      'title': 'Difteria, Tétano e Coqueluche (DTP)',
      'start': new Date(2021, 1, 7),
      'end': new Date(2021, 1, 9)
    },
    {
        'title': 'Sarampo, Rubéola, Caxumba (Tríplice Viral - SRC)',
        'start': new Date(2021, 1, 10),
        'end': new Date(2021, 1, 13)
    },
    {
        'title': 'Campanha de Vacinação contra Sarampo',
        'start': new Date(2021, 1, 11),
        'end': new Date(2021, 1, 18)
    },
    {
        'title': 'Hepatite B',
        'start': new Date(2021, 1, 23),
        'end': new Date(2021, 1, 24)
    },
    {
        'title': 'Febre Amarela',
        'start': new Date(2021, 1, 17),
        'end': new Date(2021, 1, 18)
    },
  
  ]
  

  export default events