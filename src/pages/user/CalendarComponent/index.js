import React from 'react';
import Menu from '../../../components/Menu'
import { Calendar, momentLocalizer  } from 'react-big-calendar' 
import 'react-big-calendar/lib/css/react-big-calendar.css';
import moment from 'moment'
import events from './events';
import 'moment/locale/pt-br';
moment.locale('pt-br');


function CalendarComponent() {

  const localizer = momentLocalizer(moment);

    return (
        <div className="wrapper">
        {/* Navbar */}
        <nav className="main-header navbar navbar-expand navbar-white navbar-light">
          {/* Left navbar links */}
          <ul className="navbar-nav">
            <li className="nav-item">
              <a className="nav-link" data-widget="pushmenu" href="#" role="button"><i className="fas fa-bars" /></a>
            </li>
          </ul>
        </nav>
        {/* Main Sidebar Container */}
        <Menu/>
        <div className="content-wrapper">
          {/* Content Header (Page header) */}
          <section className="content-header">
            <div className="container-fluid">
              <div className="row mb-2">
                <div className="col-sm-6">
                  <h1>Calendário</h1>
                </div>
                <div className="col-sm-6">
                  <ol className="breadcrumb float-sm-right">
                    <li className="breadcrumb-item"><a href="#">Home</a></li>
                    <li className="breadcrumb-item active">Calendário</li>
                  </ol>
                </div>
              </div>
            </div>
            {/* /.container-fluid */}
          </section>
          {/* Main content */}
          <section className="content">
            <div className="container-fluid">
              <div className="row">
                {/* /.col */}
                <div className="col-md-12">
                  <div className="card card-primary">
                    <div className="card-body p-0" style={{ height: 700 }}>
                      {/* THE CALENDAR */}
                      <Calendar 
                        events={events}
                        step={60}
                        localizer={localizer}
                        views={false}
                        navigate={false}
                        eventPropGetter={event => {
                          console.log(event);
                          if(event.title === 'Campanha de Vacinação contra Sarampo'){
                            return {style: {
                              backgroundColor: "#ff9800",
                            }}
                          }
                          else {
                          return {style: {
                            backgroundColor: "#8bc34a",
                          }}}}}
                        />
                    </div>
                    {/* /.card-body */}
                  </div>
                  {/* /.card */}
                </div>
                {/* /.col */}
              </div>
              {/* /.row */}
            </div>
            {/* /.container-fluid */}
          </section>
          {/* /.content */}
        </div>
        {/* /.content-wrapper */}
        {/* Control Sidebar */}
        <aside className="control-sidebar control-sidebar-dark">
          {/* Control sidebar content goes here */}
        </aside>
        {/* /.control-sidebar */}
      </div>
      


    );
}

export default CalendarComponent;
