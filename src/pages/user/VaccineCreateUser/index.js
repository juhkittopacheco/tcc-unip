import React from 'react';
import Menu from '../../../components/Menu'
import Select from 'react-select'
import { Link } from "react-router-dom";


const options = [
    { value: '0', label: 'Ovo' },
    { value: '9', label: 'Gelatina' }
]

function VaccineCreateUser() {
    return (
        <div className="wrapper">
  <nav className="main-header navbar navbar-expand navbar-white navbar-light">
    {/* Left navbar links */}
    <ul className="navbar-nav">
      <li className="nav-item">
        <a className="nav-link" data-widget="pushmenu" href="#" role="button"><i className="fas fa-bars" /></a>
      </li>
    </ul>
  </nav>
  {/* Main Sidebar Container */}
  <Menu />
  <div className="content-wrapper">
    {/* Content Header (Page header) */}
    <section className="content-header">
      <div className="container-fluid">
        <div className="row mb-2">
          <div className="col-sm-6">
            <h1>Minhas Vacinas</h1>
          </div>
          <div className="col-sm-6">
            <ol className="breadcrumb float-sm-right">
              <li className="breadcrumb-item"><a href="#">Home</a></li>
              <li className="breadcrumb-item active">Minhas Vacinas</li>
            </ol>
          </div>
        </div>
      </div>
      {/* /.container-fluid */}
    </section>
    {/* Main content */}
    <section className="content">
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-3">
            <div className="card">
              <div className="card-header">
                <h3 className="card-title">Informações Gerais</h3>
                <div className="card-tools">
                  <button type="button" className="btn btn-tool" data-card-widget="collapse">
                    <i className="fas fa-minus" />
                  </button>
                </div>
              </div>
              <div className="card-body p-0">
                <ul className="nav nav-pills flex-column">
                  <li className="nav-item active">
                    <a href="#" className="nav-link">
                      <i className="fas fa-copy" /> Vacinas recebidas
                      <span className="badge bg-primary float-right">12</span>
                    </a>
                  </li>
                  <li className="nav-item">
                    <a href="#" className="nav-link">
                      <i className="fas fa-exclamation-triangle" /> Não recebidas
                      <span className="badge bg-danger float-right">3</span>
                    </a>
                  </li>
                </ul>
              </div>
              {/* /.card-body */}
            </div>
            {/* /.card */}
            <div className="card">
              <div className="card-header">
                <h3 className="card-title">Dicionário</h3>
                <div className="card-tools">
                  <button type="button" className="btn btn-tool" data-card-widget="collapse">
                    <i className="fas fa-minus" />
                  </button>
                </div>
              </div>
              <div className="card-body p-0">
                <ul className="nav nav-pills flex-column">
                  <li className="nav-item">
                    <a href="#" className="nav-link">
                      <i className="fas fa-star text-warning" /> Verificadas por um orgão autorizado
                    </a>
                  </li>
                </ul>
              </div>
              {/* /.card-body */}
            </div>
            {/* /.card */}
          </div>
          {/* /.col */}
          <div className="col-md-9">
            <div className="card card-primary card-outline">
              <div className="card-header">
                <h3 className="card-title">Cadastrar vacina recebida</h3>
              </div>
              {/* /.card-header */}
              <div className="card-body">
                <div className="form-group">
                  <input className="form-control" placeholder="Local que recebeu" />
                </div>
                <div className="form-group">
                  <input className="form-control" placeholder="Nome da vacina" />
                </div>
                <div className="form-group">
                  <input className="form-control" placeholder="data que recebeu" />
                </div>
              </div>
              {/* /.card-body */}
              <div className="card-footer">
                <div className="float-right">
                  <button type="submit" className="btn btn-primary"><i className="far fa-copy" /> Salvar</button>
                </div>
                <button type="reset" className="btn btn-default"><i className="fas fa-times" /> cancelar</button>
              </div>
              {/* /.card-footer */}
            </div>
            {/* /.card */}
          </div>
          {/* /.col */}
        </div>
        {/* /.row */}
      </div>{/* /.container-fluid */}
    </section>
    {/* /.content */}
  </div>
  {/* /.content-wrapper */}
  {/* Control Sidebar */}
  <aside className="control-sidebar control-sidebar-dark">
    {/* Control sidebar content goes here */}
  </aside>
  {/* /.control-sidebar */}
</div>


    );
}

export default VaccineCreateUser;
