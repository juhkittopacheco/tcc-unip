import React from 'react';
import LoginFields from '../../../components/LoginFields'

function Login() {
    return (
        <div className="hold-transition login-page loginUnico">
            <div className="login-box">
                <div className="login-logo">
                    <a href="./index2.html"><b>Minhas</b>Vacinas</a>
                </div>
                {/* /.login-logo */}
                <LoginFields />
            </div>
            {/* /.login-box */}
        </div>

    );
}

export default Login;
