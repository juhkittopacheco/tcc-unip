import React from 'react';
import MenuHospital from '../../../components/MenuHospital'
import Select from 'react-select'


const options = [
    { value: '0', label: 'Ovo' },
    { value: '9', label: 'Gelatina' }
]

function Campaign() {
    return (
<div className="wrapper">
  {/* Navbar */}
  <nav className="main-header navbar navbar-expand navbar-white navbar-light">
    {/* Left navbar links */}
    <ul className="navbar-nav">
      <li className="nav-item">
        <a className="nav-link" data-widget="pushmenu" href="#" role="button"><i className="fas fa-bars" /></a>
      </li>
    </ul>
  </nav>
  {/* /.navbar */}
  {/* Main Sidebar Container */}
  <MenuHospital />
  <div className="content-wrapper">
    {/* Content Header (Page header) */}
    {/* Main content */}
    <section className="content">
      {/* Default box */}
      <div className="row d-flex justify-content-center">
        <div className="col-md-9">
          {/* Box Comment */}
          <div className="card card-widget">
            <div className="card-header">
              <div className="user-block">
                <img className="img-circle" src={require("../../../dist/img/hospital.png").default} alt="User Image" />
                <span className="username"><a href="#">Hospital josé</a></span>
                <span className="description">Shared publicly - 7:30 PM Today</span>
              </div>
              {/* /.user-block */}
              <div className="card-tools">
                <button type="button" className="btn btn-tool" data-toggle="tooltip" title="Mark as read">
                  <i className="far fa-circle" /></button>
                <button type="button" className="btn btn-tool" data-card-widget="collapse"><i className="fas fa-minus" />
                </button>
                <button type="button" className="btn btn-tool" data-card-widget="remove"><i className="fas fa-times" />
                </button>
              </div>
              {/* /.card-tools */}
            </div>
            {/* /.card-header */}
            <div className="card-body" style={{display: 'block'}}>
                                 
              <img className="img-fluid pad"  src={require("../../../dist/img/banner_vacinacao_sarampo_fev2020_site.png").default} alt="Photo" />
              <p>I took this photo this morning. What do you guys think?</p>
              <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at</p>
              <p>the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into
                your mouth.</p>
              <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at</p>
              <p>the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into
                your mouth.</p>
            </div>
          </div>
          {/* /.card */}
        </div>
      </div>
    </section>
    {/* /.content */}
  </div>
  {/* /.content-wrapper */}
  {/* Control Sidebar */}
  <aside className="control-sidebar control-sidebar-dark">
    {/* Control sidebar content goes here */}
  </aside>
  {/* /.control-sidebar */}
</div>

    );
}

export default Campaign;
